package net.halalaboos.poopengine;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import net.halalaboos.wilton.plugin.Plugin;

public class Interpreter extends Plugin {

	public Interpreter() {
		super();
		this.setName("Poop Interpreter");
		this.setDescription("Poop script interpreter.");
		this.setCategory("Tool");
	}
	
	@Override
	public void load() {
	}

	@Override
	public void unload() {
	}

	@Override
	protected JPanel genPanel() {
		final JPanel panel = new JPanel(new BorderLayout());
		final JTextArea textArea = new JTextArea();
		panel.add(new JScrollPane(textArea), BorderLayout.CENTER);
		panel.add(new JButton(new AbstractAction("Run") {

			@Override
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(panel, PoopEngine.runPoop(textArea.getText()), "Poop!", JOptionPane.INFORMATION_MESSAGE);
			}
			
		}), BorderLayout.SOUTH);
		return panel;
	}

}
