package net.halalaboos.color;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import net.halalaboos.wilton.plugin.Plugin;

public class ColorGen extends Plugin {

	private final Random random = new Random();
		
	private ColorDisplay colorDisplay;
	
	private JButton generate;
	
	private JTextArea textArea;
	
	private Color mainColor = Color.WHITE;

	public ColorGen() {
		super();
		this.setName("Color Gen");
		this.setDescription("Generates pastel colors");
		this.setCategory("Tool");
	}
	
	@Override
	public void load() {
		recolorPanel();
	}

	@Override
	public void unload() {
		
	}

	@Override
	protected JPanel genPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		generate = new JButton(new AbstractAction("Generate") {

			@Override
			public void actionPerformed(ActionEvent event) {
				recolorPanel();
			}
			
		});
		panel.add(generate, BorderLayout.NORTH);
		
		colorDisplay = new ColorDisplay();
		panel.add(colorDisplay, BorderLayout.CENTER);
		textArea = new JTextArea();
		textArea.setEditable(false);
		panel.add(textArea, BorderLayout.SOUTH);
		return panel;
	}
	
	protected void recolorPanel() {
		mainColor = generateColor();
		colorDisplay.recolor(mainColor);
		textArea.setText("");
		textArea.append("new Color(" + mainColor.getRed() + ", " + mainColor.getGreen() + ", " + mainColor.getBlue() + ");");
	}
	
	protected Color generateColor() {
		float hue = random.nextFloat();
		float sat = random.nextFloat();
		return Color.getHSBColor(hue, sat, 1F);
	}
	
	private class ColorDisplay extends JComponent {
		
		private Image image;
		
		private Color mainColor;
		
		public ColorDisplay() {
			super();
			this.setBorder(new TitledBorder("Example"));
			setDoubleBuffered(false);
		}

		public void recolor(Color mainColor) {
			this.mainColor = mainColor;
			repaint();
		}
		
		@Override
		public void paintComponent(Graphics graphics) {
			image = createImage(getSize().width, getSize().height);
			Graphics2D graphics2D = (Graphics2D) image.getGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			graphics2D.setFont(new Font("Verdana", Font.TRUETYPE_FONT, 18));
			draw(graphics2D);
			graphics.drawImage(image, 0, 0, null);
		}

		private void draw(Graphics2D graphics2D) {
			graphics2D.setColor(Color.WHITE);
			graphics2D.fillRect(0, 0, getSize().width, getSize().height);
			graphics2D.setColor(mainColor);
			graphics2D.drawString("The fat man eats the donut", 40, 40);
			int size = getSize().width / 3;
			graphics2D.setColor(Color.BLACK);
			graphics2D.fillRect(49, 49, size + 2, size + 2);
			graphics2D.fillOval(59 + size, 49, size + 2, size + 2);
			graphics2D.setColor(mainColor);
			graphics2D.fillRect(50, 50, size, size);
			graphics2D.fillOval(60 + size, 50, size, size);
		}
		
		
	}
	
}
