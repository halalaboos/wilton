package net.halalaboos.paint;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JColorChooser;
import javax.swing.AbstractAction;

import net.halalaboos.wilton.plugin.Plugin;

public class Paint extends Plugin {

	private final Random random = new Random();
		
	private ColorArea colorArea;
	
	private JButton clear;
	
	
	
	public Paint() {
		super();
		this.setName("Paint");
		this.setDescription("Draw stuff");
		this.setCategory("Fun");
	}
	
	@Override
	public void load() {
	}

	@Override
	public void unload() {
		
	}

	@Override
	protected JPanel genPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		colorArea = new ColorArea();
		panel.add(colorArea, BorderLayout.CENTER);
		JPanel leftPanel = new JPanel(new FlowLayout());
		clear = new JButton(new AbstractAction("Clear") {

			@Override
			public void actionPerformed(ActionEvent event) {
				colorArea.clearPanel();
			}
			
		});
		leftPanel.add(clear);
		leftPanel.add(new JButton(new AbstractAction("Color..") {

			@Override
			public void actionPerformed(ActionEvent event) {
				ColorPicker colorPicker = new ColorPicker();
			}
			
		}));
		final JSpinner sizeSpinner = new JSpinner(new SpinnerNumberModel(3, 1, 20, 1));
		sizeSpinner.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent event) {
				colorArea.setLineSize((int) sizeSpinner.getValue());
			}
			
		});
		leftPanel.add(sizeSpinner);
		/*final JSlider sizeSlider = new JSlider(JSlider.HORIZONTAL, 1, 20, 3);
		sizeSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent event) {
				colorArea.setLineSize(sizeSlider.getValue());
			}
			
		});
		sizeSlider.setPaintLabels(true);
		sizeSlider.setPaintTicks(true);
		sizeSlider.setPaintTrack(true);
		sizeSlider.setMajorTickSpacing(10);
		sizeSlider.setMinorTickSpacing(1);
		leftPanel.add(sizeSlider);*/
		panel.add(leftPanel, BorderLayout.WEST);
		return panel;
	}
	
	protected Color generateColor() {
		float hue = random.nextFloat();
		float sat = random.nextFloat();
		return Color.getHSBColor(hue, sat, 1F);
	}
	
	private class ColorPicker extends JFrame {
		
		private JColorChooser colorChooser = new JColorChooser();
		
		private ColorPicker() {
			super("Color Picker");
			this.addWindowListener(new WindowListener() {

				@Override
				public void windowActivated(WindowEvent event) {
					
				}

				@Override
				public void windowClosed(WindowEvent event) {
					
				}

				@Override
				public void windowClosing(WindowEvent event) {
					colorArea.setColor(colorChooser.getColor());
				}

				@Override
				public void windowDeactivated(WindowEvent event) {
					
				}

				@Override
				public void windowDeiconified(WindowEvent event) {
					
				}

				@Override
				public void windowIconified(WindowEvent event) {
					
				}

				@Override
				public void windowOpened(WindowEvent event) {
					
				}
				
			});
			this.add(colorChooser, BorderLayout.CENTER);
			this.add(new JButton(new AbstractAction("Choose") {

				@Override
				public void actionPerformed(ActionEvent event) {
					colorArea.setColor(colorChooser.getColor());
					dispose();
				}
				
			}), BorderLayout.SOUTH);
			this.pack();
			this.setLocationRelativeTo(null);
			this.setVisible(true);
		}
		
	}
	
	private class ColorArea extends JComponent {
		
		private final int OFFSETX = 8, OFFSETY = 8;
		
		private Image image;
		
		private Graphics2D graphics2D;
		
		private Color color;
		
		private int lastX, lastY;
						
		public ColorArea() {
			super();
			this.setBorder(LineBorder.createBlackLineBorder());
			setDoubleBuffered(false);			
			this.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent event) {
					
				}

				@Override
				public void mouseEntered(MouseEvent event) {
					
				}

				@Override
				public void mouseExited(MouseEvent event) {
					
				}

				@Override
				public void mousePressed(MouseEvent event) {
					lastX = event.getX();
					lastY = event.getY();
				}

				@Override
				public void mouseReleased(MouseEvent event) {
					
				}
				
			});
			this.addMouseMotionListener(new MouseMotionListener() {

				@Override
				public void mouseDragged(MouseEvent event) {
					graphics2D.setColor(color);
					graphics2D.drawLine(lastX - OFFSETX, lastY - OFFSETY, event.getX() - OFFSETX, event.getY() - OFFSETY);
					lastX = event.getX();
					lastY = event.getY();
					repaint();
				}

				@Override
				public void mouseMoved(MouseEvent event) {
					
				}
				
			});
		}
		
		public void setColor(Color color) {
			graphics2D.setColor(color);
		}
		
		public void setLineSize(int size) {
			graphics2D.setStroke(new BasicStroke(size));
		}
		
		public void clearPanel() {
			Graphics2D oldGraphics2D = graphics2D;
			image = createImage(getSize().width - OFFSETX * 2, getSize().height - OFFSETY * 2);
			graphics2D = (Graphics2D) image.getGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			graphics2D.setFont(new Font("Verdana", Font.TRUETYPE_FONT, 18));
			graphics2D.setStroke(oldGraphics2D.getStroke());
			graphics2D.setColor(oldGraphics2D.getColor());
			repaint();
		}

		@Override
		public void paintComponent(Graphics graphics) {
			if (image == null) {
				image = createImage(getSize().width - OFFSETX * 2, getSize().height - OFFSETY * 2);
				graphics2D = (Graphics2D) image.getGraphics();
				graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				graphics2D.setFont(new Font("Verdana", Font.TRUETYPE_FONT, 18));
				graphics2D.setStroke(new BasicStroke(3));
			}
			int borderSize = 1;
			graphics.setColor(Color.BLACK);
			graphics.fillRect(OFFSETX - borderSize, OFFSETY - borderSize, image.getWidth(null) + borderSize * 2, image.getHeight(null) + borderSize * 2);
			graphics.setColor(Color.WHITE);
			graphics.fillRect(OFFSETX, OFFSETY, image.getWidth(null), image.getHeight(null));
			graphics.drawImage(image, OFFSETX, OFFSETY, null);
		}
		
	}
	
}
