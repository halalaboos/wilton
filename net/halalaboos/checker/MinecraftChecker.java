package net.halalaboos.checker;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import net.halalaboos.wilton.ThreadManager;
import net.halalaboos.wilton.gui.WiltonGui;
import net.halalaboos.wilton.plugin.Plugin;
import net.halalaboos.wilton.util.FileUtils;
import net.halalaboos.wilton.util.MathUtils;

public class MinecraftChecker extends Plugin implements ActionListener {

	private JButton begin, pause;
	
	private boolean running = false, paused = false;
	
	private final List<LoginThread> loginThreads = new ArrayList<LoginThread>();
	
	private int threadCount = 1;
	
	private List<String> accounts = new ArrayList<String>();
	
	private List<String> workingAccounts = new ArrayList<String>();

	private final List<Proxy> proxies = new CopyOnWriteArrayList<Proxy>();
	
	private int proxyIndex = 0;
	
	private boolean useHome = true, randomProxy = false;
	
	public MinecraftChecker() {
		super();
		this.setName("Minecraft Checker");
		this.setDescription("Check your minecraft accounts");
		this.setCategory("Tool");
	}
	
	@Override
	public void load() {
	}

	@Override
	public void unload() {
	}

	@Override
	protected JPanel genPanel() {
		JPanel panel = new JPanel();
		begin = new JButton("Begin");
		begin.addActionListener(this);
		panel.add(begin);
		pause = new JButton("Pause");
		pause.setEnabled(false);
		pause.addActionListener(this);
		panel.add(pause);
		return panel;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (running) {
			if (event.getSource() == pause) {
				if (paused) {
					for (LoginThread loginThread : loginThreads)
						loginThread.resume();
					paused = false;
					pause.setName("Pause");
				} else {
					for (LoginThread loginThread : loginThreads)
						loginThread.pause();
					paused = true;
					pause.setName("Resume");			
				}
			} else if (event.getSource() == begin) {
				int response = JOptionPane.showConfirmDialog(WiltonGui.instance, "Are you sure? \n", "Currently checking..", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
				if (response == JOptionPane.YES_OPTION) {
					begin.setText("Begin");
					running = false;
					pause.setEnabled(false);
					closeThreads();
				}
			}
		} else {
			if (event.getSource() == begin) {
				running = true;
				begin.setText("Stop");
				pause.setEnabled(true);
				launchThreads();
			}
		}
	}
	
	private void launchThreads() {
		int accountPerThread = accounts.size() / threadCount, count = 0;
		if (accountPerThread < 1)
			accountPerThread = 1;
		for (int i = 0; i < threadCount; i++) {
			List<String> threadAccounts = new ArrayList<String>();
			for (int j = count; j < accountPerThread && j < accounts.size(); j++)
				threadAccounts.add(accounts.get(j));
			LoginThread loginThread = new LoginThread(this, "Login Thread #" + i, threadAccounts);
			this.loginThreads.add(loginThread);
			ThreadManager.runProcess(loginThread);
		}
	}
	
	private void closeThreads() {
		for (LoginThread loginThread : loginThreads)
			loginThread.close();
		loginThreads.clear();
	}

	public List<Proxy> loadProxies(File file) {
		return loadProxies(Proxy.Type.HTTP, file);
	}
	
	public List<Proxy> loadProxies(Proxy.Type type, File file) {
		List<String> lines = FileUtils.readFile(file);
		for (String line : lines) {
			Proxy proxy = parseProxy(type, line);
			if (proxy != null) {
				proxies.add(proxy);
			}
		}
		return proxies;
	}
	
	public void clearProxies() {
		proxies.clear();
	}
	
	public void resetProxyIndex() {
		proxyIndex = 0;
	}
	
	private Proxy parseProxy(Proxy.Type type, String line) {
		int port = 80;
		String address = line;
		if (line.contains(":")) {
			String portText = line.substring(line.indexOf(":") + 1).replaceAll(" ", "");
			if (MathUtils.isInteger(portText)) {
				port = Integer.parseInt(portText);
				address = line.substring(0, line.indexOf(":")).replaceAll(" ", "");
			}
		}
		return new Proxy(type, new InetSocketAddress(address, port));
	}
	
	public void removeBadProxy(Proxy proxy) {
		proxies.remove(proxy);
	}
	
	public Proxy nextProxy() throws Exception {
		if (randomProxy && !proxies.isEmpty()) {
			return proxies.get((int) (Math.random() * proxies.size()));
		} else {
			if (proxies.isEmpty()) {
				if (useHome)
					return Proxy.NO_PROXY;
				throw new Exception("Out of proxies");				
			}
			if (proxyIndex >= proxies.size()) {
				proxyIndex = 0;
				return nextProxy();
			} else {
				Proxy proxy = proxies.get(proxyIndex);
				proxyIndex++;
				return proxy;
			}
		}
	}
	
	public void addWorkingAccount(String account) {
		workingAccounts.add(account);
	}
	
	public void clearWorkingAccounts() {
		workingAccounts.clear();
	}

	public void removeLoginThread(LoginThread loginThread) {
		loginThreads.remove(loginThread);
		if (loginThreads.isEmpty()) {
			begin.setText("Begin");
			running = false;
			pause.setEnabled(false);
		}
	}

}
