package net.halalaboos.wilton.commands;

public interface Command {

	String[] getAliases();
	
	String getDescription();
	
	String getHelp();
	
	void run();
	
}
