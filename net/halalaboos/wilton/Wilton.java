package net.halalaboos.wilton;

import java.io.File;
import javax.swing.UIManager;
import net.halalaboos.wilton.gui.WiltonConsole;
import net.halalaboos.wilton.gui.WiltonGui;
import net.halalaboos.wilton.plugin.Plugin;
import net.halalaboos.wilton.plugin.PluginManager;
import net.halalaboos.wilton.util.Logger;

public final class Wilton {
	
	public static final String VERSION = "1.0";
	
	private static File savedir = new File(".");
	
	private static Plugin selectedPlugin;
		
	private Wilton() {
	}
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		Logger.hijackOutput();
		Wilton.load();
		WiltonConsole.instance.setup();
		WiltonGui.instance.setup();
	}

	public static void load() {
		PluginManager.loadPlugins();
		
	}
	
	public static void save() {
		PluginManager.savePlugins();
	}

	public static void updatePlugins() {
		WiltonGui.instance.updatePlugins();
	}

	public static void loadPlugin(Plugin plugin) {
		if (selectedPlugin != plugin) {
			if (selectedPlugin != null) {
				selectedPlugin.unload();
				ThreadManager.closeProcesses(selectedPlugin);
			}
			selectedPlugin = plugin;
			selectedPlugin.load();
			WiltonGui.instance.updatePluginPanel(selectedPlugin);
		}
	}
	
	public static Plugin getSelectedPlugin() {
		return selectedPlugin;
	}

	public static File getSaveDir() {
		return savedir;
	}
	
}
