package net.halalaboos.wilton.plugin;

import javax.swing.JPanel;

import net.halalaboos.wilton.Source;

public abstract class Plugin implements Source {

	private String name, author, description = "No description available.", category;
	
	private final JPanel panel;
	
	public Plugin() {
		this.panel = genPanel();
	}
		
	public abstract void load();
	
	public abstract void unload();
		
	protected abstract JPanel genPanel();

	@Override
	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	protected void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	protected void setDescription(String description) {
		this.description = description;
	}
	
	public String getCategory() {
		return category;
	}

	protected void setCategory(String category) {
		this.category = category;
	}

	public JPanel getPanel() {
		return panel;
	}

	@Override
	public String toString() {
		return name;
	}
	
}
