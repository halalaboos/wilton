package net.halalaboos.wilton.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import net.halalaboos.checker.MinecraftChecker;
import net.halalaboos.color.ColorGen;
import net.halalaboos.copy.Copy;
import net.halalaboos.fixer.Fixer;
import net.halalaboos.grabber.Grabber;
import net.halalaboos.paint.Paint;
import net.halalaboos.poopengine.Interpreter;
import net.halalaboos.wilton.Wilton;

public final class PluginManager {

	private static final File pluginFolder = new File(Wilton.getSaveDir(), "plugins");
	
	public static final List<Plugin> plugins = new ArrayList<Plugin>();
	
	private PluginManager() {
		
	}
	
	public static void registerPlugin(Plugin plugin) {
		plugins.add(plugin);
	}
	
	public static void unregisterPlugin(Plugin plugin) {
		plugins.remove(plugin);
	}
	
	public static void loadPlugins() {
		if (!pluginFolder.exists())
			pluginFolder.mkdirs();
		File[] files = pluginFolder.listFiles();
		for (File file : files) {
			if (file != null && !file.isDirectory()) {
				try {
					String classPath = null;
					JarFile jarFile = new JarFile(file);
					Enumeration<JarEntry> entries = jarFile.entries();
					for (JarEntry entry = null; entries.hasMoreElements(); ) {
						entry = entries.nextElement();
						if (entry != null && !entry.isDirectory()) {
							if (entry.getName().equalsIgnoreCase("info.txt")) {
								BufferedReader reader = new BufferedReader(new InputStreamReader(jarFile.getInputStream(entry)));
								for (String line; (line = reader.readLine()) != null; ) {
									if (line.toLowerCase().startsWith("classpath:"))
										classPath = line.substring(10);
								}
							}
						}
					}
					if (classPath != null && !classPath.isEmpty()) {
						Plugin plugin = loadFromJar(file, classPath, (Object[]) null);
						registerPlugin(plugin);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		registerPlugin(new ColorGen());
		registerPlugin(new Paint());
		registerPlugin(new Fixer());
		registerPlugin(new Grabber());
		registerPlugin(new MinecraftChecker());
		registerPlugin(new Copy());
		registerPlugin(new Interpreter());
	}
	
	
	
	private static Plugin loadFromJar(File file, String classPath, Object... args) {
		try {
			ClassLoader classLoader = URLClassLoader.newInstance(new URL[] { file.toURI().toURL() }, Wilton.class.getClassLoader());
			Class clazz = Class.forName(classPath, true, classLoader);
			return (Plugin) clazz.getConstructor().newInstance(args);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static void savePlugins() {
		
	}

	public static List<Plugin> getPlugins() {
		return plugins;
	}
	
}
