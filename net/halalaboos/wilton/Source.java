package net.halalaboos.wilton;

public interface Source {

	String getName();
	
}
