package net.halalaboos.wilton.event;

import java.util.ArrayList;
import java.util.List;

public abstract class EventManager <EVENT, LISTENER> {

	private final List<LISTENER> listeners = new ArrayList<LISTENER>();
	
	public EventManager() {
		
	}
	
	public void registerListener(LISTENER listener) {
		listeners.add(listener);
	}
	
	public void unregisterListener(LISTENER listener) {
		listeners.remove(listener);
	}
	
	public void call(EVENT event) {
		for (LISTENER listener : listeners)
			invoke(listener, event);
	}
	
	public abstract void invoke(LISTENER listener, EVENT event);
	
	public void call() {
		for (LISTENER listener : listeners)
			invoke(listener);
	}
	
	public abstract void invoke(LISTENER listener);
}
