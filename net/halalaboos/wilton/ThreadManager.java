package net.halalaboos.wilton;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.wilton.plugin.Plugin;

public final class ThreadManager {	
	
	private static final List<WiltonProcess> processes = new ArrayList<WiltonProcess>();
	
	private ThreadManager() {
	}
	
	public static void runProcess(WiltonProcess process) {
		if (!processes.contains(process)) {
			processes.add(process);
			Thread processThread = new Thread(process);
			processThread.start();
		}
	}
	
	public static void closeProcesses(Plugin plugin) {
		for (int i = 0; i < processes.size(); i++) {
			WiltonProcess process = processes.get(i);
			if (plugin == process.getSource())
				closeProcess(process);
		}
	}

	public static List<WiltonProcess> getOpenProcesses(Plugin plugin) {
		List<WiltonProcess> openProcesses = new ArrayList<WiltonProcess>();
		for (int i = 0; i < processes.size(); i++) {
			WiltonProcess process = processes.get(i);
			if (plugin == process.getSource())
				openProcesses.add(process);
		}
		return openProcesses;
	}
	
	
	public static void closeProcess(WiltonProcess process) {
		if (processes.contains(process)) {
			process.close();
			processes.remove(process);
		}
	}
}
