package net.halalaboos.wilton;

import net.halalaboos.wilton.plugin.Plugin;
import net.halalaboos.wilton.util.Logger;

public abstract class WiltonProcess implements Source, Runnable {

	private final Plugin source;
	
	private final String name;
	
	private boolean pause, running;

	public WiltonProcess(Plugin source, String name) {
		this.source = source;
		this.name = name;
		running = true;
	}
		
	@Override
	public void run() {
		while (running) {
			if (pause) {
				continue;
			} else {
				runProcess();
			}
		}
		Logger.log(this, "Exiting " + name + ".");
		ThreadManager.closeProcess(this);
	}
	
	protected abstract void runProcess();
	
	public void close() {
		running = false;
	}
	
	public void restart() {
		running = true;
	}
	
	public boolean isPause() {
		return pause;
	}

	public void pause() {
		this.pause = true;
		Logger.log(this, "Pausing " + name + ".");
	}
	
	public void resume() {
		this.pause = false;
		Logger.log(this, "Resuming " + name + ".");
	}

	public boolean isRunning() {
		return running;
	}

	public Plugin getSource() {
		return source;
	}

	@Override
	public String getName() {
		return name;
	}
	
}
