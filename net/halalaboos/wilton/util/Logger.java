package net.halalaboos.wilton.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import net.halalaboos.wilton.Source;
import net.halalaboos.wilton.gui.WiltonConsole;
import net.halalaboos.wilton.plugin.Plugin;

public final class Logger {

	private Logger() {
		
	}
	
	public static void log(Source source, String text) {
		System.out.println("[" + source.getName() + "]: " + text);
	}
	
	public static void hijackOutput() {
		try {
			PrintStream printStream = new PrintStream(new OutputStream() {
				@Override
				public final void write(int b) throws IOException {
					WiltonConsole.instance.addChar((char) b);
				}
			});
			
			System.setOut(printStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
