package net.halalaboos.wilton.gui;

import java.awt.BorderLayout;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

public final class WiltonConsole extends JFrame implements ActionListener {

	public static final WiltonConsole instance = new WiltonConsole();
	
	private final TextArea textArea = new TextArea();
	
	private final TextField textField = new TextField();
	
	private WiltonConsole() {
		super("Wilton - Console");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	public void setup() {
		this.setSize(600, 300);
		this.addComponents();
		this.setLocationRelativeTo(null);
	}
	
	private void addComponents() {
		add(new JScrollPane(textArea), BorderLayout.CENTER);
		textArea.setEditable(false);
		add(textField, BorderLayout.SOUTH);
		textField.addActionListener(this);
	}
	
	public void close() {
		this.dispose();
	}
	
	public void open() {
		this.setVisible(true);
	}
	
	public void addLine(final String line) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				textArea.append(line);
			}
			
		});
	}
	
	public void addChar(final char c) {
		/*SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				textArea.append("" + c);
			}
			
		});*/
		textArea.append("" + c);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (!textField.getText().isEmpty()) {
			textField.setText("");
		}
	}
	
}
