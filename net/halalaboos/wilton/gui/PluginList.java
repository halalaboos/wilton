package net.halalaboos.wilton.gui;

import java.awt.Font;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.halalaboos.wilton.ThreadManager;
import net.halalaboos.wilton.Wilton;
import net.halalaboos.wilton.WiltonProcess;
import net.halalaboos.wilton.plugin.Plugin;

public class PluginList extends JList<Plugin> implements ListSelectionListener {

	public PluginList() {
		setFont(new Font("Calibri", Font.PLAIN, 18));
		setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		setCellRenderer(new PluginCellRenderer());
		this.addListSelectionListener(this);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
	}

	@Override
	public void valueChanged(ListSelectionEvent event) {
		if (this.getSelectedIndex() >= 0 && this.getSelectedIndex() < this.getModel().getSize()) {
			Plugin plugin = this.getModel().getElementAt(this.getSelectedIndex());
			if (Wilton.getSelectedPlugin() != plugin) {
				List<WiltonProcess> processes = ThreadManager.getOpenProcesses(Wilton.getSelectedPlugin());
				if (!processes.isEmpty()) {
					int response = JOptionPane.showConfirmDialog(this, "Are you sure? \n There are '" + processes.size() + "' open processes!", "Open processes", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
					if (response == JOptionPane.YES_OPTION)
						Wilton.loadPlugin(plugin);
					else
						this.setSelectedIndex(event.getLastIndex());
				} else
					Wilton.loadPlugin(plugin);
			}
		}
	}
}
