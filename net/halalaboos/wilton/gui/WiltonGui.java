package net.halalaboos.wilton.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.halalaboos.wilton.plugin.Plugin;
import net.halalaboos.wilton.plugin.PluginManager;

import javax.swing.JMenuBar;

public final class WiltonGui extends JFrame {
	
	public static final WiltonGui instance = new WiltonGui();
	
	private final PluginList pluginList = new PluginList();
	
	private JMenuItem console = new JMenuItem(new AbstractAction("Toggle Console") {
		@Override
		public void actionPerformed(ActionEvent event) {
			if (WiltonConsole.instance.isShowing())
				WiltonConsole.instance.close();
			else
				WiltonConsole.instance.open();
		}
	});
	
	private JPanel oldPluginPanel = null;
	
	private WiltonGui() {
		super("Wilton");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void setup() {
		this.setSize(800, 400);
		this.addComponents();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	private void addComponents() {
		updatePlugins();
		JPanel westPanel = getPluginPanel();
		this.add(westPanel, BorderLayout.WEST);
		JMenuBar menuBar = new JMenuBar();
		JMenu optionsMenu = new JMenu("Options");
		optionsMenu.add(console);
		menuBar.add(optionsMenu);
		this.setJMenuBar(menuBar);
	}
	
	private JPanel getPluginPanel() {
		JPanel panel = new JPanel();
		panel.add(new JScrollPane(pluginList));
		return panel;
	}
	
	public void updatePluginPanel(Plugin plugin) {
		if (oldPluginPanel != null)
			this.remove(oldPluginPanel);
		this.add(plugin.getPanel(), BorderLayout.CENTER);
		this.getContentPane().revalidate();
		this.getContentPane().repaint();
		oldPluginPanel = plugin.getPanel();
	}
	
	public void updatePlugins() {
		pluginList.setListData(PluginManager.getPlugins().toArray(new Plugin[PluginManager.getPlugins().size()]));
	}
	
}
