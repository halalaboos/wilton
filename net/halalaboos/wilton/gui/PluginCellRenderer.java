package net.halalaboos.wilton.gui;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

import net.halalaboos.wilton.plugin.Plugin;


public class PluginCellRenderer extends DefaultListCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PluginCellRenderer() {
	}
	
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean selected, boolean focus) {
		Plugin plugin = (Plugin) value;
		JLabel component = (JLabel) super.getListCellRendererComponent(list, value, index, selected, focus);
		component.setText(plugin.getName());
		component.setToolTipText(plugin.getDescription());
		return component;
	}
	
}
