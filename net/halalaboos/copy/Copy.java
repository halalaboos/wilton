package net.halalaboos.copy;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.AbstractAction;

import net.halalaboos.wilton.plugin.Plugin;
import net.halalaboos.wilton.util.FileUtils;

public class Copy extends Plugin {
	
	private JTextField to, from;
	
	private JButton toSelect, fromSelect, copy;
	
	private File toFile, fromFile;
	
	public Copy() {
		super();
		this.setName("File Copier");
		this.setDescription("Copy files from a set of folders to one folder");
		this.setCategory("Tool");
	}
	
	@Override
	public void load() {
	}

	@Override
	public void unload() {
		
	}

	@Override
	protected JPanel genPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel centerPanel = new JPanel(new FlowLayout());
		centerPanel.add(new JLabel("From:"));
		from = new JTextField("");
		from.setEditable(false);
		from.setPreferredSize(new Dimension(70, 20));
		centerPanel.add(from);
		fromSelect = new JButton(new AbstractAction("Select") {

			@Override
			public void actionPerformed(ActionEvent event) {
				fromFile = chooseFolder();
				updateCopyButton();
				if (fromFile != null)
					from.setText(fromFile.getAbsolutePath());
			}
			
		});
		centerPanel.add(fromSelect);
		centerPanel.add(new JLabel("To:"));
		to = new JTextField("");
		to.setEditable(false);
		to.setPreferredSize(new Dimension(70, 20));
		centerPanel.add(to);
		toSelect = new JButton(new AbstractAction("Select") {

			@Override
			public void actionPerformed(ActionEvent event) {
				toFile = chooseFolder();
				updateCopyButton();
				if (toFile != null)
					to.setText(toFile.getAbsolutePath());
			}
			
		});
		centerPanel.add(toSelect);
		copy = new JButton(new AbstractAction("Copy") {

			@Override
			public void actionPerformed(ActionEvent event) {
				if (!toFile.exists())
					toFile.mkdirs();
				try {
					copyFiles(fromFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		});
		copy.setEnabled(false);
		panel.add(copy, BorderLayout.SOUTH);
		panel.add(centerPanel, BorderLayout.CENTER);
		return panel;
	}
	private void copyFiles(File from) throws IOException {
		File[] files = from.listFiles();
		for (File file : files) {
			if (file.isDirectory())
				copyFiles(file);
			else {
				if (file.isFile() && file.exists()) {
					File to = new File(toFile, file.getName());
					if (!to.exists())
						to.createNewFile();
					FileUtils.copyFile(to, file);
				}
			}
		}
	}
	
	private void updateCopyButton() {
		copy.setEnabled(toFile != null && fromFile != null && fromFile.exists());
	}
	
	private File chooseFolder() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.showDialog(null, "Select");
		return fileChooser.getSelectedFile();
	}
}
