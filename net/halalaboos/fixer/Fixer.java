package net.halalaboos.fixer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPanel;

import net.halalaboos.wilton.plugin.Plugin;

public class Fixer extends Plugin implements ActionListener, KeyListener {
		
	private ImagePanel imagePanel;
		
	private JTextField nameField;
	
	private JButton load, save;
	
	public Fixer() {
		super();
		this.setName("Skin Fixer");
		this.setDescription("Fix up minecraft skins");
		this.setCategory("Tool");
	}
	
	@Override
	public void load() {

	}

	@Override
	public void unload() {
		
	}

	@Override
	protected JPanel genPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		
		imagePanel = new ImagePanel();
		panel.add(imagePanel, BorderLayout.CENTER);
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new FlowLayout());
		JLabel nameLabel = new JLabel("Player name");
		topPanel.add(nameLabel);
		nameField = new JTextField("Halalaboos");
		nameField.addKeyListener(this);
		nameField.setPreferredSize(new Dimension(70, 20));
		topPanel.add(nameField);
		load = new JButton("Load");
		load.addActionListener(this);
		topPanel.add(load);
		panel.add(topPanel, BorderLayout.NORTH);
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new FlowLayout());
		save = new JButton("Save");
		save.addActionListener(this);
		bottomPanel.add(save);
		panel.add(bottomPanel, BorderLayout.SOUTH);
		
		loadSkin("Halalaboos");
	
		return panel;
	}
	

	@Override
	public void keyPressed(KeyEvent event) {
	}

	@Override
	public void keyReleased(KeyEvent event) {
			if (event.getKeyCode() == event.VK_ENTER) {
				loadSkin(nameField.getText());
			}
	}

	@Override
	public void keyTyped(KeyEvent event) {
	}
	
	private void loadSkin(String username) {
		try {
			imagePanel.loadSkin(username);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Player skin not found!", "ERROR", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private File chooseFile(String chooseMessage) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.showDialog(null, chooseMessage);
		return fileChooser.getSelectedFile();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == save) {
			File file = chooseFile("Save");
			if (file != null && !file.isDirectory()) {
				imagePanel.save(file);
			}
		} else if (event.getSource() == load) {
			loadSkin(nameField.getText());
		}
	}
}
