package net.halalaboos.fixer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class ImagePanel extends JPanel {
	
	private BufferedImage skin;
	
	private Image overlay = loadOverlay();
	
	public ImagePanel() {
		this.setPreferredSize(new Dimension(64 * 4, 32 * 4));
		this.setBorder(new TitledBorder("Skin"));
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		paintSkin(g);
	}
	
	public void loadSkin(String username) throws Exception {
		skin = ImageIO.read(new URL("http://s3.amazonaws.com/MinecraftSkins/" + username + ".png"));
		Graphics graphics = skin.getGraphics();
		graphics.drawImage(overlay, 0, 0, null);
		graphics.dispose();
		this.repaint();
	}
	
	private void paintSkin(Graphics graphics) {
		graphics.setColor(Color.WHITE);
		graphics.drawRect(0, 0, getWidth(), getHeight());
		int width = getSize().width - 40,
				height = getSize().height - 40;
		graphics.drawImage(skin, 20, 20, width, height, Color.WHITE, null);
	}
	
	private Image loadOverlay() {
		try {
			return ImageIO.read(getClass().getResourceAsStream("/res/new.png"));
		} catch (IOException e) {
			return null;
		}
	}
	
	public void save(File file) {
		try {
			if (skin != null) {
				ImageIO.write(skin, "PNG", file);
			} else {
				JOptionPane.showMessageDialog(null, "Image is null!", "ERROR", JOptionPane.ERROR_MESSAGE);
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "\"" + e.getMessage() + "\"", "ERROR", JOptionPane.ERROR_MESSAGE);
		}
	}
}
