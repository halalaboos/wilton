package net.halalaboos.grabber;

import java.awt.image.BufferedImage;

public interface SiteHandler {

	String getURL(String random, int attempt);
	
	String genRandomString();
	
	String getSiteURL();
	
	String getFileFormat(int attempt);
	
	String getImageSuffix(int attempt);
	
	boolean isValid(BufferedImage image);
	
	int getAttempts();
	
}
