package net.halalaboos.grabber.handlers;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import net.halalaboos.grabber.SiteHandler;
import net.halalaboos.grabber.utils.ImageUtils;

public class ImgurHandler implements SiteHandler {

	private final Random random = new Random();
	
	private final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	
	private final BufferedImage errorImage = loadError();

	@Override
	public String genRandomString() {
		String randomString = "";
		int length = 4 + random.nextInt(3);
		for (int i = 0; i < length; i++)
			randomString += ALPHABET.charAt(random.nextInt(ALPHABET.length()));
		return randomString;
	}

	@Override
	public String getSiteURL() {
		return "http://i.imgur.com/";
	}

	@Override
	public String getImageSuffix(int attempt) {
		switch (attempt) {
		case 0:
			return ".png";
		case 1:
			return ".jpg";
		case 2:
			return ".jpeg";
		case 3:
			return ".gif";
		default:
			return ".png";
		}
	}

	@Override
	public String getURL(String random, int attempt) {
		return "http://i.imgur.com/" + random + getImageSuffix(attempt);
	}

	@Override
	public String getFileFormat(int attempt) {
		switch (attempt) {
		case 0:
			return "PNG";
		case 1:
			return "JPG";
		case 2:
			return "JPEG";
		case 3:
			return "GIF";
		default:
			return "PNG";
		}
	}

	@Override
	public boolean isValid(BufferedImage image) {
		return !ImageUtils.imagesEqual(errorImage, image);
	}
	
	private BufferedImage loadError() {
		try {
			return ImageIO.read(getClass().getResourceAsStream("/res/error.png"));
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public int getAttempts() {
		return 3;
	}
}
