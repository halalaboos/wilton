package net.halalaboos.grabber.handlers;

import java.awt.image.BufferedImage;
import java.util.Random;


import net.halalaboos.grabber.SiteHandler;

public class PomfHandler implements SiteHandler {

	private static final Random random = new Random();
	
	private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	
	@Override
	public String genRandomString() {
		String randomString = "";
		int length = 4 + random.nextInt(3);
		for (int i = 0; i < length; i++)
			randomString += ALPHABET.charAt(random.nextInt(ALPHABET.length()));
		return randomString;
	}

	@Override
	public String getSiteURL() {
		return "http://a.pomf.se/";
	}

	@Override
	public String getImageSuffix(int attempt) {
		switch (attempt) {
		case 0:
			return ".png";
		case 1:
			return ".jpg";
		case 2:
			return ".jpeg";
		case 3:
			return ".gif";
		default:
			return ".png";
		}
	}

	@Override
	public String getURL(String random, int attempt) {
		return "http://a.pomf.se/" + random + getImageSuffix(attempt);
	}

	@Override
	public String getFileFormat(int attempt) {
		switch (attempt) {
		case 0:
			return "PNG";
		case 1:
			return "JPG";
		case 2:
			return "JPEG";
		case 3:
			return "GIF";
		default:
			return "PNG";
		}
	}

	@Override
	public boolean isValid(BufferedImage image) {
		return true;
	}

	@Override
	public int getAttempts() {
		return 3;
	}
}