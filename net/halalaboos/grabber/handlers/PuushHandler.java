package net.halalaboos.grabber.handlers;

import java.awt.image.BufferedImage;
import java.util.Random;


import net.halalaboos.grabber.SiteHandler;

public class PuushHandler implements SiteHandler {

	private static final Random random = new Random();
	
	private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	
	@Override
	public String genRandomString() {
		String randomString = "";
		int length = 5;
		for (int i = 0; i < length; i++)
			randomString += ALPHABET.charAt(random.nextInt(ALPHABET.length()));
		return randomString;
	}

	@Override
	public String getSiteURL() {
		return "http://puu.sh/";
	}

	@Override
	public String getImageSuffix(int attempt) {
		return "";
	}

	@Override
	public String getURL(String random, int attempt) {
		return "http://puu.sh/" + random;
	}

	@Override
	public String getFileFormat(int attempt) {
		return "PNG";
	}

	@Override
	public boolean isValid(BufferedImage image) {
		return true;
	}

	@Override
	public int getAttempts() {
		return 0;
	}
}