package net.halalaboos.grabber;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;

import net.halalaboos.grabber.handlers.ImgurHandler;
import net.halalaboos.grabber.handlers.PomfHandler;
import net.halalaboos.grabber.handlers.PuushHandler;
import net.halalaboos.wilton.ThreadManager;
import net.halalaboos.wilton.plugin.Plugin;

public class Grabber extends Plugin implements ActionListener, KeyListener {
	
	private JButton download;
	
	private JTextField amountField, threadsField;
	
	private JLabel notification;
	
	private JComboBox<String> imageHandlers;
	
	private boolean running = false;
	
	private int threadCount = 0, count = -1, amount = 50, threads = 1;
	
	private File saveDirectory = null;
		
	private final SiteHandler[] handlers = new SiteHandler[] {
		new ImgurHandler(),
		new PomfHandler(),
		new PuushHandler()
	};

	public Grabber() {
		super();
		this.setName("Image Grabber");
		this.setDescription("Grabs images from the interwebs");
		this.setCategory("Tool");
	}
	

	@Override
	public void load() {
	}

	@Override
	public void unload() {
	}

	@Override
	protected JPanel genPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new GridLayout(3, 2));

		JLabel amountLabel = new JLabel("Amount");
		topPanel.add(amountLabel);
		amountField = new JTextField("50");
		amountField.addKeyListener(this);
		topPanel.add(amountField);
		
		JLabel threadsLabel = new JLabel("Threads");
		topPanel.add(threadsLabel);
		threadsField = new JTextField("1");
		threadsField.addKeyListener(this);
		topPanel.add(threadsField);
		
		notification = new JLabel("Status: Idle");
		topPanel.add(notification);
		
		imageHandlers = new JComboBox<String>(new String[] { "Imgur", "Pomf", "Puush" });
		topPanel.add(imageHandlers);
		
		panel.add(topPanel, BorderLayout.NORTH);

		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new FlowLayout());
		download = new JButton("Download");
		download.addActionListener(this);
		bottomPanel.add(download);
		panel.add(bottomPanel, BorderLayout.SOUTH);
		
		return panel;
	}
	
	@Override
	public void keyPressed(KeyEvent event) {
	}

	@Override
	public void keyReleased(KeyEvent event) {
		if (event.getSource() == amountField) {
			try {
				amount = Integer.parseInt(amountField.getText());
			} catch (Exception e) {
				event.consume();
			}
		} else if (event.getSource() == threadsField) {
			try {
				threads = Integer.parseInt(threadsField.getText());
			} catch (Exception e) {
				event.consume();
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent event) {
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == download) {
			if (this.saveDirectory == null)
				saveDirectory = chooseFolder();
			if (saveDirectory != null) {
				saveDirectory.mkdirs();
				for (int i = 0; i < threads; i++)
					ThreadManager.runProcess(new ThreadDownload("Image Download #" + i, this, handlers[imageHandlers.getSelectedIndex()]));
			}
		}
	}

	public JTextField getUrl() {
		return amountField;
	}

	public boolean isRunning() {
		return running;
	}
	
	private File chooseFolder() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.showDialog(null, "Select");
		return fileChooser.getSelectedFile();
	}

	public void setRunning(boolean running) {
		this.running = running;
		if (running) {
			download.setEnabled(false);
			amountField.setEnabled(false);
			imageHandlers.setEnabled(false);
			threadsField.setEnabled(false);
			count = 0;
		} else {
			download.setEnabled(true);
			amountField.setEnabled(true);
			imageHandlers.setEnabled(true);
			threadsField.setEnabled(true);
			count = -1;
		}
	}	

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public File getSaveDirectory() {
		return saveDirectory;
	}
	
	public void updateNotification() {
		if (count == -1)
			notification.setText("Status: Idle");
		else {
			notification.setText("Status: " + count + " / " + amount);
		}
	}
	
	public void incrementCount() {
		count++;
	}

	public int getCount() {
		return count;
	}

	public void onThreadDie(ThreadDownload threadDownload) {
		threadCount++;
		if (threadCount >= threads) {
			setRunning(false);
		}
	}
}
