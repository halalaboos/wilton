package net.halalaboos.grabber;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

import net.halalaboos.wilton.WiltonProcess;

public class ThreadDownload extends WiltonProcess {

	private final Grabber grabber;
	
	private final SiteHandler handler;

	public ThreadDownload(String name, Grabber grabber, SiteHandler handler) {
		super(grabber, name);
		grabber.setRunning(true);
		this.grabber = grabber;
		this.handler = handler;
	}
	
	public void download() {
		download(handler.genRandomString(), 0);
	}
	
	public void download(String randomName, int attempt) {
		try {
			URL url = new URL(handler.getURL(randomName, attempt));
			BufferedImage image = read(url);
			if (shouldContinue()) {
				if (image == null) {
					if (attempt < handler.getAttempts()) {
						download(randomName, attempt + 1);
					} else
						download();
				} else {
					if (handler.isValid(image)) {
						this.save(randomName, handler.getFileFormat(attempt), image);
					} else {
						if (attempt < handler.getAttempts()) {
							download(randomName, attempt + 1);
						} else
							download();
					}
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	boolean shouldContinue() {
		return grabber.getCount() < grabber.getAmount() || grabber.getAmount() == -1;
	}
	
	private BufferedImage read(URL url) {
		try {
			return ImageIO.read(url);
		} catch (IOException e) {
			return null;
		}
	}
	
	private void save(String name, String format, BufferedImage image) {
		try {
			if (shouldContinue()) {
				if (!ImageIO.write(image, format, new File(grabber.getSaveDirectory(), name + "." + format.toLowerCase()))) {
					System.out.println("No format found " + format);
				} else
					grabber.incrementCount();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void runProcess() {
		download();
		grabber.updateNotification();
		if (!shouldContinue()) {
			grabber.onThreadDie(this);
			grabber.updateNotification();
			this.close();
		}
	}
	
}
